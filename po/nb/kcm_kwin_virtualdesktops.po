# Translation of kcm_kwin_virtualdesktops to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2007, 2009, 2010, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: kcm_kwindesktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-31 02:17+0000\n"
"PO-Revision-Date: 2014-08-09 08:38+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Bjørn Steensrud"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bjornst@skogkatt.homelinux.org"

#: desktopsmodel.cpp:191
#, kde-format
msgctxt "A numbered name for virtual desktops"
msgid "Desktop %1"
msgid_plural "Desktop %1"
msgstr[0] ""
msgstr[1] ""

#: desktopsmodel.cpp:479
#, kde-format
msgid "There was an error connecting to the compositor."
msgstr ""

#: desktopsmodel.cpp:678
#, kde-format
msgid "There was an error saving the settings to the compositor."
msgstr ""

#: desktopsmodel.cpp:681
#, kde-format
msgid "There was an error requesting information from the compositor."
msgstr ""

#: package/contents/ui/main.qml:89
#, kde-format
msgctxt "@info:tooltip"
msgid "Rename"
msgstr ""

#: package/contents/ui/main.qml:100
#, kde-format
msgctxt "@info:tooltip"
msgid "Confirm new name"
msgstr ""

#: package/contents/ui/main.qml:108
#, kde-format
msgctxt "@info:tooltip"
msgid "Remove"
msgstr ""

#: package/contents/ui/main.qml:135
#, kde-format
msgid ""
"Virtual desktops have been changed outside this settings application. Saving "
"now will overwrite the changes."
msgstr ""

#: package/contents/ui/main.qml:151
#, kde-format
msgid "Row %1"
msgstr ""

#: package/contents/ui/main.qml:164
#, kde-format
msgctxt "@action:button"
msgid "Add"
msgstr ""

#: package/contents/ui/main.qml:182
#, kde-format
msgid "1 Row"
msgid_plural "%1 Rows"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:198
#, kde-format
msgid "Options:"
msgstr ""

#: package/contents/ui/main.qml:200
#, kde-format
msgid "Navigation wraps around"
msgstr ""

#: package/contents/ui/main.qml:218
#, kde-format
msgid "Show animation when switching:"
msgstr ""

#: package/contents/ui/main.qml:269
#, kde-format
msgid "Show on-screen display when switching:"
msgstr ""

#: package/contents/ui/main.qml:288
#, kde-format
msgid "%1 ms"
msgstr ""

#: package/contents/ui/main.qml:312
#, kde-format
msgid "Show desktop layout indicators"
msgstr "Vis indikatorer for skrivebordsutforming"
